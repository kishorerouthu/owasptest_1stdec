var _WM_APP_PROPERTIES = {
  "activeTheme" : "material",
  "defaultLanguage" : "en",
  "displayName" : "OwaspTest_1stDec",
  "homePage" : "Main",
  "name" : "OwaspTest_1stDec",
  "platformType" : "WEB",
  "supportedLanguages" : "en",
  "type" : "APPLICATION",
  "version" : "1.0"
};